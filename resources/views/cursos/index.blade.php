@extends('layaouts.layaout')

@section('title', 'Cursos')

@section('content')    
    <h1>Bienvenido a la página principal de cursos</h1>

    <a href="{{ route('cursos.create') }}">Crear un curso</a>
    <ul>
        @foreach ($cursos as $curso)
            <li>
                <a href="{{ route('cursos.show', $curso) }}">{{ $curso->name }}</a>
            </li>
        @endforeach
    </ul>

    {{ $cursos->links() }} {{-- De esta forma paginamos todos los datos de nuestra colección --}}
@endsection
