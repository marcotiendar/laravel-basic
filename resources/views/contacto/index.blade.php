@extends('layaouts.layaout')

@section('title', 'Contacto')

@section('content')
	<h1>Déjanos un mensaje</h1>

  <form action="{{ route('contacto.store') }}" method="POST">
    @csrf

    <label>
      Nombre:
      <br>
      <input type="text" name="name">
    </label>
    <br>

    @error('name')
      <p><strong>{{ $message }}</strong></p>
    @enderror

    <label>
      Correo:
      <br>
      <input type="email" name="correo">
    </label>
    <br>

    @error('correo')
      <p><strong>{{ $message }}</strong></p>
    @enderror

    <label>
      Mensaje:
      <br>
      <textarea name="mensaje" rows="5"></textarea>
    </label>

    @error('mensaje')
      <p><strong>{{ $message }}</strong></p>
    @enderror

    <br>
    <button type="submit">Enviar mensaje</button>
  </form>

  @if (session('info'))
    <script>
      alert("{{ session('info') }}");
    </script>
  @endif
@endsection
