<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>@yield('title')</title> {{-- La propiedad yield hará que el contenido sea variable en cada página --}}
  <!-- favicon -->
  <!-- estilos -->
  <style>
    * {
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif
    }

    .active {
      color: red;
      font-weight: bold;
    }
  </style>
</head>
<body>
  @include('layaouts.partials.header')
  @yield('content')

  <!-- footer -->
  @include('layaouts.partials.footer')
</body>
</html>
