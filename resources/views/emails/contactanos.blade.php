<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <style>
    * {
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    }

    body {
      background-color: #2E3440;
      color: #E5E9F0;
    }
  </style>
  <title>Correo de prueba</title>
</head>
<body>
  <h1>Correo electrónico</h1>
  <p>Correo de pruebas por laravel</p>

  <p><strong>Nombre: </strong>{{ $contacto['name'] }}</p>
  <p><strong>Correo: </strong>{{ $contacto['correo'] }}</p>
  <p><strong>Mensaje: </strong>{{ $contacto['mensaje'] }}</p>

</body>
</html>
