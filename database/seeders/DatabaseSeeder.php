<?php

namespace Database\Seeders;

use App\Models\Curso;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    User::factory(10)->create();
    Curso::factory(50)->create(); // Forma recomendada de trabajar por laravel

    // Metodo call de clase seeder
    // $this->call(CursoSeeder::class); // <- Ejecuta las instancias de la clase que indico en el archivo de seeders
  }
}
