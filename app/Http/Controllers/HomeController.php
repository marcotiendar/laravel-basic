<?php

namespace App\Http\Controllers; // <- Lugar donde se encuentra el archivo

use Illuminate\Http\Request;

class HomeController extends Controller
{
  /**
   * Ocupamos el método __invoke, cuando queremos que el controlador
   * invoke una única ruta
   */
  public function __invoke()
  {
    return view('home');
  }
}
